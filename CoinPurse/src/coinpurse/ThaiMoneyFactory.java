package coinpurse;

/**
 * An Thai money factory which create Thai money.
 * @author Chinthiti Wisetsombat
 *
 */
public class ThaiMoneyFactory extends MoneyFactory{
	
	/** Currency for money in Thailand. */
	public static final String CURRENCY = "Baht";
	
	@Override
	Valuable createMoney(double value) {
		String val = Double.toString(value);
		switch(val){
		case "1.0": case "2.0": case "5.0": case "10.0": 
			return new Coin(value, CURRENCY);
		case "20.0": case "50.0": case "100.0": case "500.0": case "1000.0": 
			return new BankNote(value, CURRENCY);
		default:
			throw new IllegalArgumentException();
		}
		
	}

}
