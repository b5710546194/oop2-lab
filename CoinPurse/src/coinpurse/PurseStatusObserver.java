package coinpurse;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRootPane;
import javax.swing.JWindow;
import javax.swing.SwingConstants;
import javax.swing.border.MatteBorder;

/**
 * A GUI for displaying the purse's status.
 * @author Chinthiti Wisetsombat
 *
 */
public class PurseStatusObserver implements Observer , Runnable{

	JWindow window;
	JLabel statusLabel;
	JProgressBar progressBar;
	JPanel panel;
	
	/**
	 * Constructor for PurseStatusObserver.
	 * @param purseCapacity capacity of the purse that will be displayed.
	 */
	public PurseStatusObserver(int purseCapacity){
		initComponent(purseCapacity);
	}
	
	/**
	 * Initialize components of this GUI.
	 * @param purseCapacity capacity of the purse that will be displayed.
	 */
	public void initComponent(int purseCapacity){	
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		window = new JWindow();
		window = new JWindow();
		window.setBounds((int)screenSize.getWidth()-430,(int)screenSize.getHeight()-160,400,60);
		Container pane = window.getContentPane();
		panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		pane.add(panel);
		
		statusLabel = new JLabel("EMPTY");
		statusLabel.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		statusLabel.setFont(new Font("Gabriola",Font.BOLD,25));
		statusLabel.setForeground(new Color(222,88,219));
		panel.add(statusLabel);
		
		progressBar = new JProgressBar(SwingConstants.HORIZONTAL, 0 , purseCapacity);
		panel.add(progressBar);
		progressBar.setBorderPainted(false);
		JRootPane rootPane = window.getRootPane();
		rootPane.setBorder(new MatteBorder(2,2,2,2,Color.GRAY));
		panel.setBackground(new Color(105,224,132));
		
	}

	/**
	 * Run thi GUI.
	 */
	@Override
	public void run() {
		window.setVisible(true);
	}

	/**
	 * Update this GUI when purse is changed.
	 * @param obj an object that implements Observable.
	 * @param info information of that object.
	 */
	@Override
	public void update(Observable obj, Object info) {
		if(obj instanceof Purse){
			Purse purse = (Purse)obj;
			if(purse.isFull()){ statusLabel.setText("FULL"); }
			
			else if(purse.count()==0){ statusLabel.setText("EMPTY");}
			
			else {statusLabel.setText(purse.count()+"");}
			progressBar.setValue(purse.count());
		
		}
	}
}
