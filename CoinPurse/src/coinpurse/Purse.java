package coinpurse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Observable;

import strategy.WithdrawStrategy;

/**
 *  A purse contains money(coins, banknotes, and coupons).
 *  You can insert money, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the purse decides which
 *  money to remove.
 *  
 *  @author Chinthiti Wisetsombat
 */
public class Purse extends Observable{
	/** ValueComparator for Collections.sort. */
	private final ValueComparator VALUE_COMPARATOR = new ValueComparator();
	/** Collection of money in the purse. */
	private ArrayList<Valuable> money;
	/** Strategy for withdrawal */
	private WithdrawStrategy strategy;
	/** Capacity is maximum NUMBER of coins the purse can hold.
	 *  Capacity is set when the purse is created.
	 */
	private int capacity;

	/** 
	 *  Create a purse with a specified capacity.
	 *  @param capacity is maximum number of money you can put in purse.
	 */
	public Purse( int capacity ) {
		this.capacity = capacity;
		money = new ArrayList<Valuable>(capacity);
	}

	/**
	 * Count and return the number of money in the purse.
	 * This is the number of money, not their value.
	 * @return the number of money in the purse
	 */
	public int count(){
		return this.money.size();
	}

	/** 
	 *  Get the total value of all items in the purse.
	 *  @return the total value of items in the purse.
	 */
	public double getBalance() {
		double balance = 0;
		for(Valuable value: money){
			balance += value.getValue();
		}
		return balance;
	}


	/**
	 * Return the capacity of the purse.
	 * @return the capacity
	 */

	public int getCapacity() {
		return this.capacity;
	}

	/** 
	 *  Test whether the purse is full.
	 *  The purse is full if number of items in purse equals
	 *  or greater than the purse capacity.
	 *  @return true if purse is full.
	 */
	public boolean isFull() {
		if(this.capacity == money.size()){
			return true;
		}
		return false;
	}

	/** 
	 * Insert money into the purse.
	 * The money is only inserted if the purse has space for it
	 * and the money has positive value.  No worthless money!
	 * @param value is the object that implements Valuable
	 * @return true if coin inserted, false if can't insert
	 */
	public boolean insert( Valuable value ) {
		if(isFull()){
			return false;
		}
		if(value.getValue() <= 0){
			return false;
		}
		money.add(value);
		Collections.sort(money,VALUE_COMPARATOR);
		super.setChanged();
		super.notifyObservers(this);
		return true;
	}
	
	/**
	 * Set a strategy for withdrawal.
	 * @param strategy a new strategy
	 */
	public void setWithdrawStrategy(WithdrawStrategy strategy){
		this.strategy = strategy;
	}

	/**  
	 *  Withdraw the requested amount of money.
	 *  Return an array of money withdrawn from purse,
	 *  or return null if cannot withdraw the amount requested.
	 *  @param amount is the amount to withdraw
	 *  @return array of money for money withdrawn, or null if cannot withdraw requested amount.
	 */
	public Valuable[] withdraw( double amount ) {
		Valuable[] withdrewMoney = strategy.withdraw(amount,money);
		if(withdrewMoney != null){
			for(Valuable valuable: withdrewMoney){
				money.remove(valuable);
			}
		}
		super.setChanged();
		super.notifyObservers(this);
		return withdrewMoney;
	}

	/** 
	 * toString returns a string description of the purse contents.
	 * It can return whatever is a useful description.
	 * @return total value and total number of coin in the purse.
	 */
	@Override
	public String toString() {
		String s = "";
		s += count() + " coins with value ";
		s += getBalance();
		return s;
	}

}