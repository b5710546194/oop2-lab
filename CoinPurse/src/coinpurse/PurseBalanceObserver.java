package coinpurse;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JLabel;
import javax.swing.JRootPane;
import javax.swing.JWindow;
import javax.swing.border.MatteBorder;

/**
 * A GUI for displaying balance of a purse.
 * @author Chinthiti Wisetsombat
 *
 */
public class PurseBalanceObserver implements Observer,Runnable{

	JWindow window;
	JLabel balanceLabel;
	
	/**
	 * Constructor for PurseBalanceObserver.
	 */
	public PurseBalanceObserver(){
		initComponent();
	}
	
	/**
	 * Run this GUI.
	 */
	@Override
	public void run() {
		window.setVisible(true);
		
		
	}

	/**
	 * Update an information when purse is changed
	 * @param obj an object that implements Observable.
	 * @param info information of that object.
	 */
	@Override
	public void update(Observable obj, Object info) {
		if(obj instanceof Purse){
			Purse purse = (Purse)obj;
			int balance = (int)purse.getBalance();
			balanceLabel.setText("Purse's balance: " + balance+ " Baht");
		}
		
	}
	
	/**
	 * Initialize components of this GUI.
	 */
	public void initComponent(){
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		window = new JWindow();
		window.setBounds((int)screenSize.getWidth()-430,(int)screenSize.getHeight()-100,400,50);		
		
		JRootPane pane = window.getRootPane();
		pane.setBorder(new MatteBorder(2,2,2,2,Color.BLUE));
		pane.setBackground(new Color(26,149,223));
		pane.setLayout(new FlowLayout());
		
		balanceLabel = new JLabel("Purse's balance: " + "0 Baht");
		balanceLabel.setFont(new Font("Segoe Script",Font.BOLD,23));
		balanceLabel.setForeground(new Color(255,255,255));
		pane.add(balanceLabel);
	}
	
}
