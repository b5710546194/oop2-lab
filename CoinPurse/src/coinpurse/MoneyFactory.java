package coinpurse;

import java.util.ResourceBundle;

/**
 * An abstract MoneyFactory which print Thai money or Malaysian money.
 * @author Chinthiti Wisetsombat
 *
 */
public abstract class MoneyFactory {
	/** An instance of MoneyFactory */
	private static MoneyFactory instance = null;
	/** Default setting of the MoneyFactory */
	private static MoneyFactory defaultFactory = new ThaiMoneyFactory();
	
	
	/**
	 * Return an instance of MoneyFactory according to the properties file.
	 * @return an instance of MoneyFacrory.
	 */
	static MoneyFactory getInstance(){
		if(instance == null){
			ResourceBundle bundle = ResourceBundle.getBundle("factory");
			String factoryType = bundle.getString("factoryType");
			try {
				instance = (MoneyFactory) Class.forName(factoryType).newInstance();
			} catch (InstantiationException | IllegalAccessException
					| ClassNotFoundException e) {
				System.out.println("Error creating MoneyFactory " + e.getMessage());
				return defaultFactory;
			}
		}
		return instance;
	}
	
	/** 
	 * Create money (coin or banknote) depend on the country.
	 * @param value is the value of money that will be created.
	 * @return money in form of coin or banknote depend on the country.
	 */  
	abstract Valuable createMoney(double value);
	
	/**
	 * Create money (coin or banknote) depend on the country.
	 * @param value is the value of money that will be created.
	 * @return money in form of coin or banknote depend on the country.
	 */
	Valuable createMoney(String value){
		return instance.createMoney(Double.parseDouble(value));
	}
	
	
}
