package coinpurse;

import java.util.Comparator;

/**
 * A ValueComparator is used to compare two value.
 * @author Chinthiti Wisetsombat
 *
 */
public class ValueComparator implements Comparator<Valuable>{
	
	/**
	 * compare 2 valuable.
	 * @param a first valuable
	 * @param b second valuable
	 * @return return < 0 if first valuable is less than second one, > 0 if first valuable is greater than second one ,or 0 if two valuable is equal
	 */
	public int compare(Valuable a,Valuable b){
		return (int)Math.signum(a.getValue()-b.getValue());
	}
}
