package coinpurse;

/**
 * A coupon with the color.
 * @author Chinthiti Wisetsombat
 */
public class Coupon extends AbstractValuable {
	
	private double value;
	private String color;
	
	/** enum for coupon color*/
	private enum CouponType {
		RED(100.0),BLUE(50.0),GREEN(20.0);
		
		private double value;
		private CouponType(final double value){
			this.value = value;
		}

		public double getValue(){
			return value;
		}
	}
	
	
	/**
	 * Constructor for a new coupon.
	 * @param color color of the coupon
	 */
	public Coupon (String color, String currency){
		super(currency);
		CouponType couponType = CouponType.valueOf(color.toUpperCase());
		this.value = couponType.getValue();
		this.color = color;
	}
	
	
	/**
	 * get the value of the coupon.
	 * @return value of the coupon
	 */
	public double getValue(){
		return this.value;
	}
	
	/**
	 * get the color of this coupon.
	 * @return color of this coupon
	 */
	public String getColor(){
		return this.color;
	}
	
	/**
	 * give the information of the coupon.
	 * @return coupon's information
	 */
	public String toString(){
		return this.getColor() + " coupon";
	}
	
	
}
