package coinpurse;

/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Chinthiti Wisetsombat
 */
public class Coin extends AbstractValuable {

	/** Value of the coin. */
	private double value;

	/** 
	 * Constructor for a new coin. 
	 * @param value is the value for the coin
	 * @param currency is the currency of this coin.
	 */
	public Coin( double value, String currency ) {
		super(currency);
		this.value = value;
	}


	/**
	 * Method for get value of Coin object.
	 * @return value of the coin
	 */
	public double getValue(){
		return value;
	}


	/**
	 * Method that return a string with a value of this coin.
	 * @return value of the coin in String
	 */
	public String toString(){
		return (int)value + " " + super.getCurrency() + " coin";
	}

}

