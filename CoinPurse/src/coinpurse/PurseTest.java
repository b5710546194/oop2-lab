package coinpurse;

import strategy.GreedyWithdraw;
import strategy.RecursiveWithdraw;

/**
 * test class for Purse.
 * @author Chinthiti Wisetsombat
 *
 */
public class PurseTest {
	
	public static void main(String[]args){
		Purse purse = new Purse(5);
		purse.setWithdrawStrategy(new RecursiveWithdraw());
		Coin coin1 = new Coin(10);
		Coupon coupon1 = new Coupon ("blue");
		System.out.println(purse.insert(coin1));
		System.out.println(purse.insert(coupon1));
		System.out.println(purse.getBalance());
		System.out.println(purse.count());
		Valuable[] stuff = purse.withdraw(60);
		printValuableArray(stuff);
		purse.getBalance();
		
		BankNote bankNote1 = new BankNote(100.0);
		BankNote bankNote2 = new BankNote(50.0);
		BankNote bankNote3 = new BankNote(20.0);
		
		Coupon coupon2 = new Coupon("red");
		Coupon coupon3 = new Coupon("green");
		System.out.println("--------------------------");
		Purse purse1 = new Purse(5);
		purse1.setWithdrawStrategy(new RecursiveWithdraw());
		purse1.insert(bankNote2);
		purse1.insert(bankNote3);
		purse1.insert(bankNote1);
		purse1.insert(coupon1);
		purse1.insert(coupon2);
		
		stuff = purse1.withdraw(320);
		
		printValuableArray(stuff);
		
		System.out.println(purse1.getBalance());
	}
	
	/**
	 * print information from array.
	 * @param valuableArr Array of objects that implements Valuable
	 */
	public static void printValuableArray(Valuable[] valuableArr){

		for(Valuable value : valuableArr){

			System.out.println(value.toString());
		}
	}
}
