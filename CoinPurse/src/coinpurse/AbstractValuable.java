package coinpurse;

/**
 * 
 * @author Chinthiti Wisetsombat
 */
public abstract class AbstractValuable implements Valuable{
	
	/** Currency of this AbstractValueable */
	private String currency;
	
	/**
	 * Consturctor for AbstractValuable which has currency.
	 * @param currency is the currency of money
	 */
	public AbstractValuable(String currency){
		this.currency = currency;
	}
	

	/**
	 * return value of the objects that implement Valuable interface.
	 * @return value
	 */
	public abstract double getValue();
	
	/**
	 * compare two valuables.
	 * @param obj other valuable that will be compared
	 * @return <0 if first value is lees than the second, >0 if first value is greater than the second, or 0 if both value is equal
	 */
	public int compareTo(Valuable obj){
		if(obj == null) return -1;
		return (int) Math.signum(this.getValue() - obj.getValue());
	}
	
	/**
	 * check that two objects are the same type and they are equal.
	 * @param obj another that will be checked
	 * @return true if two objects are the same type and are equal, otherwise return false.
	 */
	public boolean equals(Object obj){
		if(obj==null){
			return false;
		}
		if(obj.getClass()==Coin.class){
			Coin other = (Coin)obj;
			return this.getValue() == other.getValue();
			
		}else if(obj.getClass()== BankNote.class){
			BankNote other = (BankNote)obj;
			return this.getValue() == other.getValue();
			
		}else if(obj.getClass()==Coupon.class){
			Coupon other = (Coupon) obj;
			return this.getValue() == other.getValue();
		}
		return false;
		
	}
	
	/**
	 * Return currency of this AbstractValuable
	 * @return currency pf thos AbstractValuable
	 */
	public String getCurrency() {
		return currency;
	}
}
