package coinpurse;

/**
 * An Malaysian money factory which create malaysian money. 
 * @author Chinthiti Wisetsombat
 *
 */
public class MalaiMoneyFactory extends MoneyFactory{

	/** Currency of coins in Malaysia. */
	public static final String COIN_CURRENCY = "Sen";
	/** Currency of banknotes in Malaysia. */
	public static final String BANKNOTE_CURRENCY = "Ringgit";
	
	@Override
	Valuable createMoney(double value) {
		String val = Double.toString(value);
		switch(val){
		case "0.05": case "0.10": case "0.20": case "0.50": 
			return new Coin (value*100, COIN_CURRENCY );
		case "1.0": case "2.0": case "5.0": case "10.0": 
		case "20.0": case "50.0": case "100.0": 
			return new BankNote (value, BANKNOTE_CURRENCY);
		default:
			throw new IllegalArgumentException();
		}
	}


}
