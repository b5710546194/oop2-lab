package coinpurse;

/**
 * A Banknote with a monetary value.
 * @author Chinthiti Wisetsombat
 *
 */
public class BankNote extends AbstractValuable {
	
	private static int nextSerialNumber = 1000000;
	private double value;
	private int serialNumber;
	
	/**
	 * Constructor for a new BankNote.
	 * @param value is value of the new BankNote
	 * @param currency is the currency of this banknote.
	 */
	public BankNote(Double value, String currency){
		super(currency);
		this.value = value;
		this.serialNumber = getNextSerialNumber() ;
	}
	

	/**
	 * give the unique serial number for a banknote.
	 * @return a unique serial number for banknote
	 */
	public static int getNextSerialNumber(){
		return nextSerialNumber++;
	}
	
	/**
	 * give the serial number of Banknote's instance.
	 * @return serial number of BankNote
	 */
	public int getSerialNumber(){
		return serialNumber;
	}
	
	/**
	 * give the value of Banknote's instance.
	 * @return value of BankNote
	 */
	public double getValue(){
		return this.value;
	}
	
	/**
	 * give the information of this banknote.
	 * @return value of this banknote and serial
	 */
	public String toString(){
		return getValue()+ " " + super.getCurrency() + " Banknote ["+ getSerialNumber() + "]";
	}
}
