package coinpurse;

/**
 * 
 * @author Chinthiti Wisetsombat
 */
public interface Valuable extends Comparable<Valuable>{
	
	/** get the value from the object.
	 *  @return value of object.
	 */
	public double getValue();
}
