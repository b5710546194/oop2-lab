package coinpurse;

import java.util.Arrays;

import strategy.GreedyWithdraw;
import strategy.RecursiveWithdraw;


/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 * @author Chinthiti Wisetsombat
 */

public class Main {
	private static int CAPACITY = 10;
	/**
	 * @param args not used
	 */
	public static void main( String[] args ) {
		
		
		//Example of difference between GreedyWithdraw and RecursiveWithdraw
		System.out.println("Example of difference between GreedyWithdraw and RecursiveWithdraw\n");
		Valuable[] money = new Valuable[] {new Coin(5),new Coin(3), new Coin(2), new Coin(2)};


		Purse purseWithRecursive = new Purse(CAPACITY);
		purseWithRecursive.setWithdrawStrategy(new RecursiveWithdraw());

		Purse purseWithGreedy = new Purse(CAPACITY);
		purseWithGreedy.setWithdrawStrategy(new GreedyWithdraw());

		for(Valuable valuable: money){
			System.out.println("insert " + valuable.toString() + "to greedy withdraw purse and recursive withdraw purse");
			purseWithRecursive.insert(valuable);
			purseWithGreedy.insert(valuable);
		}

		Valuable[] withdrewFromGreedy = purseWithGreedy.withdraw(9);
		Valuable[] withdrewFromRecursive = purseWithRecursive.withdraw(9);

		System.out.print("Withdraw 9 baht from greedy withdraw purse : ");
		if(withdrewFromGreedy == null){
			System.out.println("can't withdraw 9 baht from this purse");
		}else System.out.println(Arrays.toString(withdrewFromGreedy));

		System.out.print("Withdraw 9 baht from recursive withdraw purse : ");
		if(withdrewFromRecursive == null){
			System.out.println("can't withdraw 9 baht from this purse");
		}else System.out.println(Arrays.toString(withdrewFromRecursive));

		System.out.printf("\n --  End of the example  --\n\n\n");
		// End of examples.
		
		
		// create a Purse
		Purse purse = new Purse(CAPACITY);
		purse.setWithdrawStrategy(new RecursiveWithdraw());
		
		// create balance observer
		PurseBalanceObserver balanceObs = new PurseBalanceObserver();
		balanceObs.run();
		purse.addObserver(balanceObs);
		
		//create status observer
		PurseStatusObserver statusObs = new PurseStatusObserver(CAPACITY);
		statusObs.run();
		purse.addObserver(statusObs);
		
		
		
		
		// create a ConsoleDialog with a reference to the Purse object
		ConsoleDialog consoleDialog = new ConsoleDialog(purse);
		// run() the ConsoleDialog
		consoleDialog.run();
	}
}

