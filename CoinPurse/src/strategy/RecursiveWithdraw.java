 package strategy;

import java.util.Arrays;
import java.util.List;

import coinpurse.Valuable;

/**
 * A recursive withdraw strategy for purse.
 * @author Chinthiti Wisetsombat.
 *
 */
public class RecursiveWithdraw implements WithdrawStrategy{
	
	/**
	 * Withdraw money from a list with specific amount of money.
	 * @param amount amount of money that will be withdrew.
	 * @param valuables list of money that will be withdrew.
	 * @return array of money that is withdrew.
	 */
	@Override
	public Valuable[] withdraw(double amount, List<Valuable> valuables) {
		return tryToWithdraw(amount, valuables);
	}
	
	//HELPER METHOD
	private Valuable[] tryToWithdraw(double withdrawAmount, List<Valuable> valuables){
		Valuable valuable = valuables.get(valuables.size()-1);
		double value = valuable.getValue();
		Valuable[] withdrewMoney = new Valuable[valuables.size()];
	    
	    if(value == withdrawAmount){
	    	return new Valuable[] {valuable};
	    }else if(valuables.size() == 1 || withdrawAmount == 0){
	    	return null;
	    }
	    
	    
	    Valuable[] pickThis = tryToWithdraw(withdrawAmount - value , valuables.subList(0 , valuables.size()-1));
	    
	    Valuable[] notPickThis = tryToWithdraw(withdrawAmount , valuables.subList(0, valuables.size()-1));
		
	    if(pickThis != null){
	    	Valuable[] temp = Arrays.copyOf(pickThis, pickThis.length+1);
	    	temp[temp.length-1] = valuable;
	    	return temp;
	    }
	    return notPickThis;
	}
}
