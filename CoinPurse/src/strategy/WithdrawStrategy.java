package strategy;

import java.util.List;

import coinpurse.Valuable;

/**
 * An interface for implementation withdraw money from a list.
 * @author Chinthiti Wisetsombat
 *
 */
public interface WithdrawStrategy {
	/**
	 * Withdraw money from a list. (items won't be remove from the List)
	 * @param amount amount of money that will be withdrew
	 * @param valuables List of money
	 * @return list of money that is withdrew
	 */
	public Valuable[] withdraw(double amount, List<Valuable> valuables);
}
