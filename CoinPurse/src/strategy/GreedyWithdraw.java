package strategy;

import java.util.ArrayList;
import java.util.List;

import coinpurse.Valuable;

/**
 * A greedy withdraw strategy for purse.
 * @author Chinthiti Wisetsombat
 *
 */
public class GreedyWithdraw implements WithdrawStrategy {

	@Override
	/**
	 * Withdraw money from the list with specific amount.
	 * @param amount amount of money that will be withdrew.
	 * @param list of money that will be withdrew.
	 * @return arrays of money that is withdrew.
	 */
	public Valuable[] withdraw(double amount, List<Valuable> valuables) {
		
		ArrayList<Valuable> tempMoney = (ArrayList<Valuable>)((ArrayList<Valuable>)valuables).clone();
		ArrayList<Valuable> withdrewMoney = new ArrayList<Valuable>();
		
		int index = tempMoney.size()-1;
		
		if(amount < 0){
			return null;
		}
		
		while(amount>0&&index != -1){
			Valuable tempValue = tempMoney.get(index);
			if(amount - tempValue.getValue() >= 0){
				amount -= tempValue.getValue();
				withdrewMoney.add(tempValue);
				tempMoney.remove(index);
			}
			index--;
		}
		
		if(amount > 0){
			return null;
		}
		
		Valuable[] moneyArr = new Valuable[withdrewMoney.size()];
		withdrewMoney.toArray(moneyArr);
	
		return moneyArr;
	}

}
